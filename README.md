Simple Blog App (Offline) with MVVM + Android Architecture Components + Koin
============================================================================

This is a simple offline blogging app where users will be able to write a post.

### Functionality
The app is composed of 5 main screens.
#### Home
Displays the list of posts

![Scheme](images/1.jpg)

#### Add/Edit Post
Displays the adding/editing of a post

![Scheme](images/2.jpg)

#### Profile
Displays the user profile along with it's post

![Scheme](images/3.jpg)

#### Login
Displays the the login screen

![Scheme](images/4.jpg)

#### Sign up
Displays the the signup screen

![Scheme](images/5.jpg)

### Libraries
* [AndroidX Library][androidx]
* [Android Architecture Components][arch]
* [Android Data Binding][data-binding]
* [Navigation][navigation] for navigating screens
* [Koin][koin] for dependency injection
* [Glide][glide] for image loading


[androidx]: https://developer.android.com/jetpack/androidx
[arch]: https://developer.android.com/arch
[data-binding]: https://developer.android.com/topic/libraries/data-binding/index.html
[navigation]: https://developer.android.com/guide/navigation
[koin]: https://insert-koin.io/
[glide]: https://github.com/bumptech/glide
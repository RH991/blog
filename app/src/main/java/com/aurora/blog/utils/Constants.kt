package com.aurora.blog.utils

/**
 * Constants used throughout the app.
 */
const val DATABASE_NAME = "blog-db"
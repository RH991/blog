package com.aurora.blog.extensions

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.widget.Toast
import androidx.annotation.StringRes
import com.aurora.blog.utils.StringUtils.EMPTY
import com.aurora.blog.views.add.AddPostFragment
import java.io.FileNotFoundException


/**
 * Show the given string resource as toast message
 *
 * @param value the string resource
 */
fun Context.toast(@StringRes value: Int) {
    Toast.makeText(this, value, Toast.LENGTH_LONG).show()
}

/**
 * Resolve the value as a resource then show the {@code msg} as toast message
 */
fun Context.resolveAndToast(value: String) {
    val msg = resolveStringResource(value).let {
        if (it.isEmpty()) {
            value
        } else {
            it
        }
    }

    Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
}

/**
 * Resolve the {@code value} as a resource
 */
fun Context.resolveStringResource(value: String): String {
    var ret = EMPTY
    try {
        ret = getString(
            resources.getIdentifier(
                value,
                "string", packageName
            )
        )
    } catch (ex: Resources.NotFoundException) {
        // do nothing
    }
    return ret
}

/**
 * Retrieves image bitmap from Uri
 */
fun Context.getImageFrom(uri: Uri): Bitmap? {
    return try {
        contentResolver.openFileDescriptor(uri, AddPostFragment.READ)?.use {
            BitmapFactory.decodeFileDescriptor(it.fileDescriptor)
        }
    } catch (e: FileNotFoundException) {
        null
    }

}
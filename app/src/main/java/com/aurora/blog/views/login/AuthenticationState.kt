package com.aurora.blog.views.login

enum class AuthenticationState {
        /** initial state */
        UNAUTHENTICATED,
        /** authenticated */
        AUTHENTICATED,
        /** authentication failed */
        INVALID_AUTHENTICATION
}
package com.aurora.blog.views.signup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.aurora.blog.data.Status
import com.aurora.blog.data.User
import com.aurora.blog.data.UserRepository
import com.aurora.blog.utils.SingleLiveEvent
import com.aurora.blog.views.BaseViewModel
import com.aurora.blog.views.login.AuthenticationState
import kotlinx.coroutines.launch

class SignupViewModel(
    private val userRepo: UserRepository
) : BaseViewModel() {
    val email: MutableLiveData<String> = MutableLiveData()
    val userName: MutableLiveData<String> = MutableLiveData()
    val password: MutableLiveData<String> = MutableLiveData()

    /** assumes that all fields are not valid*/
    val validationWatcher: MutableLiveData<Int> = SingleLiveEvent()

    private val _authenticationState = SingleLiveEvent<AuthenticationState>()
    val authenticationState: LiveData<AuthenticationState>
        get() = _authenticationState

    private val _loginClicked = SingleLiveEvent<Boolean>()
    val loginClicked: LiveData<Boolean>
        get() = _loginClicked

    init {
        _authenticationState.value = AuthenticationState.UNAUTHENTICATED
    }

    fun sinUp() {
        if (validationWatcher.value ?: 0 >= VALID_COUNT) {
            viewModelScope.launch {
                /** try sign up */
                val result = userRepo.signUp(
                    User(email = email.value!!,
                        userName = userName.value!!, password = password.value!!))
                /** evaluate result*/
                when(result.status) {
                    Status.SUCCESS -> _authenticationState.value = AuthenticationState.AUTHENTICATED
                    Status.ERROR -> _msg.value = result.code?.code
                }
            }
        }
    }

    fun onLoginClicked() {
        _loginClicked.value = true
    }

    companion object {
        /** used for validation, reflects the number of fields to validate*/
        const val VALID_COUNT = 3
    }
}
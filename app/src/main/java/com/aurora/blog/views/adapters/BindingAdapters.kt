package com.aurora.blog.views.adapters

import android.net.Uri
import android.text.format.DateUtils
import android.util.Patterns.EMAIL_ADDRESS
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.addTextChangedListener
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import com.aurora.blog.R
import com.aurora.blog.extensions.getImageFrom
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import java.io.File


/**
 * View binding adapters
 */

const val ZERO_MINUTES_AGO = "0 minutes ago"

/**
 * Loads {@code imageUrl} via glide into [AppCompatImageView]
 */
@BindingAdapter("bindImage")
fun AppCompatImageView.bindImage(imageUrl: String?) {
    imageUrl?.run {
        Glide.with(context)
            .load(context.getImageFrom(Uri.parse(imageUrl)))
            .into(this@bindImage)
    }
}

/**
 * Show or hides the view
 *
 * @param isGone indicates that the view should be shown if [true], otherwise [false]
 */
@BindingAdapter("isGone")
fun View.bindIsGone(isGone: Boolean) {
    visibility = if (isGone) {
        View.GONE
    } else {
        View.VISIBLE
    }
}

/**
 * Toggles between two images depending on the value of privacy
 *
 * @param isPrivate the boolean flag to choose what to display
 */
@BindingAdapter("isPrivate")
fun AppCompatImageView.bindIsPrivate(isPrivate: Boolean) {
    Glide.with(context).load(
        if (isPrivate) R.drawable.ic_private
        else R.drawable.ic_public
    ).into(this)
}

/**
 * Formats {@code time} via [DateUtils.getRelativeTimeSpanString]
 * and set as text value
 */
@BindingAdapter("bindTimeSpan")
fun AppCompatTextView.bindTimeSpan(time: Long) {
    val temp = DateUtils.getRelativeTimeSpanString(
        time,
        System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS
    )

    text = if (time == 0L || temp == ZERO_MINUTES_AGO) {
        context.getString(R.string.now)
    } else {
        temp
    }
}

/**
 * Validator for user name
 */
@BindingAdapter("userNameValidator")
fun TextInputLayout.bindUserNameValidator(watcher: MutableLiveData<Int>) {
    editText?.addTextChangedListener {
        error = null
        if (it.isNullOrEmpty()) {
            val err = context.getString(R.string.err_username_required)
            if (err != error) error = err
            report(false, watcher)
        } else {
            error = null
            report(true, watcher)
        }
    }
}

/**
 * Validator for password
 */
@BindingAdapter("passwordValidator")
fun TextInputLayout.bindPasswordValidator(watcher: MutableLiveData<Int>) {
    editText?.addTextChangedListener {
        error = null
        when {
            it.isNullOrEmpty() -> {
                val err = context.getString(R.string.err_password_required)
                if (err != error) error = err
                report(false, watcher)
            }
            it.length < 8 -> {
                val err = context.getString(R.string.err_password_too_short)
                if (err != error) error = err
                report(false, watcher)
            }
            else -> {
                error = null
                report(true, watcher)
            }
        }
    }
}

/**
 * Validator for email
 */
@BindingAdapter("emailValidator")
fun TextInputLayout.bindEmailValidator(watcher: MutableLiveData<Int>) {
    editText?.addTextChangedListener {
        error = null
        when {
            it.isNullOrEmpty() -> {
                val err = context.getString(R.string.err_email_required)
                if (err != error) error = err
                report(false, watcher)
            }
            !EMAIL_ADDRESS.matcher(it).matches() -> {
                val err = context.getString(R.string.err_email_invalid)
                if (err != error) error = err
                report(false, watcher)
            }
            else -> {
                error = null
                report(true, watcher)
            }
        }
    }
}


/**
 * Title validator
 */
@BindingAdapter("titleValidator")
fun TextInputLayout.bindTitleValidator(watcher: MutableLiveData<Int>) {
    editText?.addTextChangedListener {
        error = null
        when {
            it.isNullOrEmpty() -> {
                val err = context.getString(R.string.err_title_required)
                if (err != error) error = err
                report(false, watcher)
            }
            else -> {
                error = null
                report(true, watcher)
            }
        }
    }
}

/** check the marker tag if already  being reported, otherwise do not report to watcher
 * this will be used to count the number of valid fields*/
fun TextInputLayout.report(add: Boolean, watcher: MutableLiveData<Int>) {
    val t = getTag(R.dimen.tag_key)
    if (t == null && add) {
        watcher.value = watcher.value?.plus(1) ?: 1
        setTag(R.dimen.tag_key, 1)
    } else if (t != null && !add) {
        watcher.value = watcher.value?.minus(1)
        setTag(R.dimen.tag_key, null)
    }
}


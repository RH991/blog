package com.aurora.blog.views

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.aurora.blog.utils.SingleLiveEvent


/**
 * Base class for viewmodel with helper properties
 */
abstract class BaseViewModel: ViewModel() {

    /**
     * Property for showing message
     */
    protected val _msg = SingleLiveEvent<String>()
    val msg: LiveData<String>
        get() = _msg
}
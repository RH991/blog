package com.aurora.blog.views.home


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.aurora.blog.R
import com.aurora.blog.data.UserPost
import com.aurora.blog.databinding.FragmentHomeBinding
import com.aurora.blog.views.adapters.GenericAdapter
import com.aurora.blog.views.dialogs.MoreBottomFragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import androidx.appcompat.app.AlertDialog


/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {

    private val binding: FragmentHomeBinding by lazy {
        FragmentHomeBinding.inflate(layoutInflater)
    }

    private val postAdapter by lazy {
        GenericAdapter(
            R.layout.layout_post_item,
            object : GenericAdapter.OnItemClickListener<UserPost> {
                override fun onItemClicked(view: View, item: UserPost) {
                    when (view.id) {
                        R.id.iv_more -> MoreBottomFragment(getMenuListener(item)).show(
                            requireActivity()
                                .supportFragmentManager,
                            BottomSheetDialogFragment::class.java.simpleName
                        )
                        R.id.iv_avatar,
                        R.id.tv_user -> findNavController().navigate(
                            HomeFragmentDirections
                                .actionHomeFragmentToProfileFragment(item.userName)
                        )
                    }
                }
            }, arrayOf(R.id.iv_more, R.id.iv_avatar, R.id.tv_user)
        ).apply { setHasStableIds(true) }
    }

    private val viewModel: HomeViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding.apply {
            viewModel = this@HomeFragment.viewModel
            lifecycleOwner = viewLifecycleOwner
            rvList.adapter = postAdapter
            rvList.setHasFixedSize(true)
            rvList.setItemViewCacheSize(20)
        }

        subscribeUI()

        return binding.root
    }

    private fun subscribeUI() {
        viewModel.apply {
            refresh()
            showAddPost.observe(viewLifecycleOwner, Observer {
                findNavController().navigate(R.id.action_homeFragment_to_addPostFragment)
            })

            posts.observe(viewLifecycleOwner, Observer {
                binding.hasPost = it.isNotEmpty()
                postAdapter.submitList(it)
            })
        }
    }

    fun getMenuListener(item: UserPost) = object : MoreBottomFragment.OnItemClickListener {
        override fun onEditPostClicked() {
            findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToAddPostFragment(item))
        }

        override fun onDeletePostClicked() {
            AlertDialog.Builder(requireContext(), R.style.AlertDialogTheme).apply {
                setMessage(R.string.delete_post_alert)
                setPositiveButton(R.string.yes) { _, _ ->
                    viewModel.deletePost(item.post.postId)
                }
                setNegativeButton(R.string.no, null)
            }.create().show()
        }
    }
}

package com.aurora.blog.views.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.aurora.blog.BR

/**
 * A generic adapter that can be used for all {@link RecyclerView}
 * adapter that also implements {@link DiffUtil#ItemCallback}
 *
 * @param resId the resource id for the layout
 * @param itemClickListener the click listener that will handle when the item is clicked
 * @param
 */
class GenericAdapter<T : DiffCallback>(
    @LayoutRes private val resId: Int,
    private val itemClickListener: OnItemClickListener<T>,
    private val idsToBindClickListener: Array<Int> = emptyArray())
    : ListAdapter<T, GenericAdapter.ViewHolder<T>>(DiffCallbackImpl()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(DataBindingUtil.inflate(
        LayoutInflater.from(parent.context), resId, parent, false),
            itemClickListener, idsToBindClickListener)

    override fun onBindViewHolder(holder: ViewHolder<T>, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).getId().hashCode().toLong()
    }
    class ViewHolder<T>(
        private val binding: ViewDataBinding,
        private val itemClickListener: OnItemClickListener<T>,
        private val idsToBindClickListener: Array<Int>
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: T) {
            binding.apply {
                setVariable(BR.item, item)
                (binding.root as ViewGroup).children.forEach {
                    if (idsToBindClickListener.contains(it.id)) {
                        it.setOnClickListener {v ->
                            itemClickListener.onItemClicked(v, item)
                        }
                    }
                }
                executePendingBindings()
            }
        }
    }

    interface OnItemClickListener<T> {
        fun onItemClicked(view: View, item: T)
    }
}

interface DiffCallback {
    fun getId(): String
    override fun equals(other: Any?) : Boolean
}

private class DiffCallbackImpl<T : DiffCallback> : DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(oldItem: T, newItem: T) = oldItem.getId() == newItem.getId()

    override fun areContentsTheSame(oldItem: T, newItem: T) = oldItem == newItem
}
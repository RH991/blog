package com.aurora.blog.views.profile


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.view.forEach
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.aurora.blog.R
import com.aurora.blog.data.UserPost
import com.aurora.blog.databinding.FragmentProfileBinding
import com.aurora.blog.extensions.toast
import com.aurora.blog.views.adapters.GenericAdapter
import com.aurora.blog.views.dialogs.MoreBottomFragment
import com.aurora.blog.views.home.HomeFragmentDirections
import com.aurora.blog.views.login.AuthenticationState
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment() {

    private val args: ProfileFragmentArgs by navArgs()

    private val binding: FragmentProfileBinding by lazy {
        FragmentProfileBinding.inflate(layoutInflater)
    }

    private val postAdapter by lazy {
        GenericAdapter(R.layout.layout_post_item,
            object : GenericAdapter.OnItemClickListener<UserPost> {
                override fun onItemClicked(view: View, item: UserPost) {
                    when(view.id) {
                        R.id.iv_more -> MoreBottomFragment(getMenuListener(item)).show(
                            requireActivity()
                                .supportFragmentManager,
                            BottomSheetDialogFragment::class.java.simpleName
                        )
                    }
                }
            }, arrayOf(R.id.iv_more, R.id.iv_avatar, R.id.tv_user)).apply { setHasStableIds(true) }
    }

    private val viewModel: ProfileViewModel by viewModel {
        parametersOf(args.argumentUserName)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding.apply {
            viewModel = this@ProfileFragment.viewModel
            lifecycleOwner = viewLifecycleOwner
            rvList.adapter = postAdapter
            rvList.setHasFixedSize(true)
            rvList.setItemViewCacheSize(20)
        }

        subscribeUI()
        setHasOptionsMenu(true)

        return binding.root
    }

    private fun subscribeUI() {
        viewModel.apply {
            refresh()
            showAddPost.observe(viewLifecycleOwner, Observer {
                findNavController().navigate(R.id.action_profileFragment_to_addPostFragment)
            })

            posts.observe(viewLifecycleOwner, Observer {
                binding.hasPost = it.isNotEmpty()
                postAdapter.submitList(it)
            })

            authenticationState.observe(viewLifecycleOwner, Observer {
                if (it == AuthenticationState.UNAUTHENTICATED) {
                    findNavController().navigate(R.id.action_profileFragment_to_loginFragment)
                }
            })

            showUserMenu.observe(viewLifecycleOwner, Observer {
                if (it) {
                    binding.toolbar.apply {
                        setOnMenuItemClickListener { item ->
                            when (item.itemId) {
                                R.id.menu_logout -> {
                                    logout().observe(viewLifecycleOwner, Observer {
                                        if (it) {
                                            findNavController().popBackStack(R.id.homeFragment, false)
                                        } else {
                                            requireContext().toast(R.string.err_generic)
                                        }
                                    })
                                    true
                                }
                                else -> {
                                    super.onOptionsItemSelected(item)
                                }
                            }
                        }

                        setNavigationOnClickListener {
                            findNavController().navigateUp()
                        }
                    }
                } else {
                    binding.toolbar.menu.clear()
                    requireActivity().invalidateOptionsMenu()
                }
            })
        }
    }

    fun getMenuListener(item: UserPost) = object : MoreBottomFragment.OnItemClickListener {
        override fun onEditPostClicked() {
            findNavController().navigate(ProfileFragmentDirections.actionProfileFragmentToAddPostFragment(item))
        }

        override fun onDeletePostClicked() {
            AlertDialog.Builder(requireContext(), R.style.AlertDialogTheme).apply {
                setMessage(R.string.delete_post_alert)
                setPositiveButton(R.string.yes) { _, _ ->
                    viewModel.deletePost(item.post.postId)
                }
                setNegativeButton(R.string.no, null)
            }.create().show()
        }
    }
}

package com.aurora.blog.views.add


import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.aurora.blog.R
import com.aurora.blog.databinding.FragmentAddPostBinding
import com.aurora.blog.extensions.getImageFrom
import com.aurora.blog.extensions.toast
import com.aurora.blog.views.login.AuthenticationState
import com.bumptech.glide.Glide
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


/**
 * A simple [Fragment] subclass.
 */
class AddPostFragment : Fragment() {

    private val args : AddPostFragmentArgs by navArgs()

    private val binding: FragmentAddPostBinding by lazy {
        FragmentAddPostBinding.inflate(layoutInflater)
    }

    private val viewModel: AddPostViewModel by viewModel {
        parametersOf(args.argumenUserPost)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding.apply {
            viewModel = this@AddPostFragment.viewModel
            lifecycleOwner = this@AddPostFragment
        }
        subscribeUI()
        setHasOptionsMenu(true)

        return binding.root
    }

    private fun subscribeUI() {
        viewModel.apply {
            showImagePicker.observe(viewLifecycleOwner, Observer {
                if (it) {
                    createImagePickerIntent()
                }
            })

            binding.toolbar.apply {
                setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.menu_post -> {
                            post().observe(viewLifecycleOwner, Observer {
                                if (it) {
                                    findNavController().navigateUp()
                                } else {
                                    requireContext().toast(R.string.err_add_post)
                                }
                            })
                            true
                        }
                        else -> {
                            super.onOptionsItemSelected(item)
                        }
                    }
                }

                setNavigationOnClickListener {
                    findNavController().navigateUp()
                }

                authenticationState.observe(viewLifecycleOwner, Observer {
                    if (it == AuthenticationState.UNAUTHENTICATED) {
                        findNavController().navigate(R.id.action_addPostFragment_to_loginFragment)
                    }
                })
            }
        }
    }

    private fun createImagePickerIntent() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            // show only files that can be open
            addCategory(Intent.CATEGORY_OPENABLE)
            // filter
            type = IMAGE_FORMAT
        }
        startActivityForResult(intent, REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            data?.data?.run {
                Glide.with(requireContext())
                    .load(requireContext().getImageFrom(this))
                    .into(binding.ivImage)
                viewModel.image.value = this.toString()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_login, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_login -> {
                viewModel.post()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    companion object {
        const val IMAGE_FORMAT = "image/*"
        const val REQUEST_CODE = 999
        const val READ = "r"
    }
}

package com.aurora.blog.views.dialogs

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.aurora.blog.R
import com.aurora.blog.databinding.BottomSheetMoreBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


/**
 * A simple [BottomSheetDialogFragment] subclass.
 */
class MoreBottomFragment(private val listener: OnItemClickListener) : BottomSheetDialogFragment() {

    private val binding: BottomSheetMoreBinding by lazy {
        BottomSheetMoreBinding.inflate(layoutInflater)
    }

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding.layoutEditPost.setOnClickListener {
            listener.onEditPostClicked()
            dismiss()
        }
        binding.layoutDeletePost.setOnClickListener {
            listener.onDeletePostClicked()
            dismiss()
        }
        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        return super.onCreateDialog(savedInstanceState)
    }

    interface OnItemClickListener {
        /** edit */
        fun onEditPostClicked()
        /** delete */
        fun onDeletePostClicked()
    }
}

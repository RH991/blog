package com.aurora.blog.views.profile

/**
 * Describes what type of user is checking
 * the screen right now. This will dictates
 * what data will the user be shown
 */
enum class UserScreenState {
        /** all owners post*/
        ALL_OWNED,
        /** all public post of a user*/
        PUBLIC_OWNED,
        /** all public post and owners post*/
        ALL_PUBLIC_AND_OWNED
}
package com.aurora.blog.views.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.aurora.blog.data.UserRepository
import com.aurora.blog.utils.SingleLiveEvent
import com.aurora.blog.views.BaseViewModel
import kotlinx.coroutines.launch

class LoginViewModel(
    private val userRepo: UserRepository
) : BaseViewModel() {

    /** input fields */
    val userName: MutableLiveData<String> = MutableLiveData()
    val password: MutableLiveData<String> = MutableLiveData()
    /** assumes that all fields are not valid*/
    val validationWatcher: MutableLiveData<Int> = SingleLiveEvent()

    private val _authenticationState = SingleLiveEvent<AuthenticationState>()
    val authenticationState: LiveData<AuthenticationState>
        get() = _authenticationState

    private val _signUpClicked = SingleLiveEvent<Boolean>()
    val signUpClicked: LiveData<Boolean>
        get() = _signUpClicked

    init {
        _authenticationState.value = AuthenticationState.UNAUTHENTICATED
    }

    fun login() {
        if (validationWatcher.value == VALID_COUNT) {
            viewModelScope.launch {
                _authenticationState.postValue(if (userRepo.isPasswordValidForUser(
                        userName.value!!, password.value!!
                    ) == true
                ) { AuthenticationState.AUTHENTICATED
                } else {
                    AuthenticationState.INVALID_AUTHENTICATION
                })
            }
        }
    }

    fun onSignUpClicked() {
        _signUpClicked.value = true
    }

    companion object {
        /** used for validation, reflects the number of fields to validate*/
        const val VALID_COUNT = 2
    }
}
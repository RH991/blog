package com.aurora.blog.views.signup


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.aurora.blog.R
import com.aurora.blog.databinding.FragmentSignUpBinding
import com.aurora.blog.extensions.resolveAndToast
import com.aurora.blog.views.login.AuthenticationState
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * A simple [Fragment] subclass.
 */
class SignupFragment : Fragment() {

    private val binding: FragmentSignUpBinding by lazy {
        FragmentSignUpBinding.inflate(layoutInflater)
    }

    private val viewModel: SignupViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding.apply {
            viewModel = this@SignupFragment.viewModel
            lifecycleOwner = viewLifecycleOwner
        }

        subscribeUI()
        setHasOptionsMenu(true)

        return binding.root
    }

    private fun subscribeUI() {
        viewModel.apply {
            authenticationState.observe(viewLifecycleOwner, Observer {
                when (it) {
                    AuthenticationState.AUTHENTICATED -> {
                        findNavController().run {
                            popBackStack(R.id.loginFragment, true)
                            navigateUp()
                        }
                    }
                }
            })

            msg.observe(viewLifecycleOwner, Observer {
                requireContext().resolveAndToast(it)
            })

            loginClicked.observe(viewLifecycleOwner, Observer {
                findNavController().navigateUp()
            })

            binding.toolbar.apply {
                setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.menu_signup -> {
                            sinUp()
                            true
                        }
                        else -> {
                            super.onOptionsItemSelected(item)
                        }
                    }
                }

                setNavigationOnClickListener {
                    findNavController().navigateUp()
                }
            }
        }
    }
}

package com.aurora.blog.views.login


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.aurora.blog.R
import com.aurora.blog.databinding.FragmentLoginBinding
import com.aurora.blog.extensions.toast
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : Fragment() {

    private val binding: FragmentLoginBinding by lazy {
        FragmentLoginBinding.inflate(layoutInflater)
    }

    private val viewModel: LoginViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding.apply {
            viewModel = this@LoginFragment.viewModel
            lifecycleOwner = viewLifecycleOwner
        }

        subscribeUI()
        setHasOptionsMenu(true)

        return binding.root
    }

    private fun subscribeUI() {
        viewModel.apply {
            authenticationState.observe(viewLifecycleOwner, Observer {
                when (it) {
                    AuthenticationState.AUTHENTICATED -> findNavController().popBackStack(R.id.homeFragment, false)
                    AuthenticationState.INVALID_AUTHENTICATION -> requireContext().toast(R.string.err_invalid_username_password)
                }
            })

            signUpClicked.observe(viewLifecycleOwner, Observer {
                findNavController().navigate(R.id.action_loginFragment_to_signupFragment)
            })

            binding.toolbar.apply {
                setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.menu_login -> {
                            login()
                            true
                        }
                        else -> {
                            super.onOptionsItemSelected(item)
                        }
                    }
                }
            }
        }
    }
}

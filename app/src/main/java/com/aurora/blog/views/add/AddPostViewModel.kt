package com.aurora.blog.views.add

import androidx.lifecycle.*
import com.aurora.blog.data.*
import com.aurora.blog.utils.SingleLiveEvent
import com.aurora.blog.utils.StringUtils.EMPTY
import com.aurora.blog.views.login.AuthenticationState

class AddPostViewModel(
    private val userRepo: UserRepository,
    private val postRepo: PostRepository,
    private val userPost: UserPost? = null
) : ViewModel() {

    private val _authenticationState = MediatorLiveData<AuthenticationState>()
    val authenticationState: LiveData<AuthenticationState>
        get() = _authenticationState

    /** assumes that all fields are not valid*/
    val validationWatcher: MutableLiveData<Int> = SingleLiveEvent()

    val title: MutableLiveData<String> = MutableLiveData()
    val content: MutableLiveData<String> = MutableLiveData()
    val image: MutableLiveData<String> = MutableLiveData()
    val isPrivate: MutableLiveData<Boolean> = MutableLiveData()
    val userInfo = userRepo.getLoggedInUser()

    private val _showImagePicker = SingleLiveEvent<Boolean>()
    val showImagePicker: LiveData<Boolean>
        get() = _showImagePicker

    init {
        val source = userRepo.getLoggedInUser()
        _authenticationState.apply {
            addSource(source) {
                removeSource(source)
                if (it != null) {
                    _authenticationState.value = AuthenticationState.AUTHENTICATED
                } else {
                    _authenticationState.value = AuthenticationState.UNAUTHENTICATED
                }
            }
        }
        /** edit mode */
        userPost?.also {
            title.value = it.post.title
            content.value = it.post.content
            image.value = it.getSingleImage()
            isPrivate.value = it.post.isPrivate
            validationWatcher.value = 1
        }
    }

    fun post(): LiveData<Boolean> = liveData {
        if (validationWatcher.value ?: 0 >= VALID_COUNT) {
            userPost?.run { /** edit */
                val post = userPost.post.let {
                    Post(it.postId, it.fkUserId, title.value ?: EMPTY,
                        content.value ?: EMPTY, isPrivate.value ?: false,
                        it.isActive, it.dateCreated)
                }
                emit(postRepo.updatePost(post))
            } ?: userInfo.value?.run {/** add */
                postRepo.addPost(
                    UserPost(
                        post = Post(
                            fkUserId = userId, title = title.value!!,
                            content = content.value ?: EMPTY, isPrivate = isPrivate.value ?: false
                        ),
                        images = if (image.value == null) emptyList()
                        else listOf(
                            PostImage(
                                fkPostId = userId,
                                position = 1, uri = image.value!!
                            )
                        )
                    )
                ).also { emit(it) }
            } ?: emit(false)
        }
    }

    fun onImageAddClicked() {
        _showImagePicker.value = true
    }

    companion object {
        /** used for validation, reflects the number of fields to validate*/
        const val VALID_COUNT = 1
    }
}
package com.aurora.blog.views.profile

import androidx.lifecycle.*
import com.aurora.blog.data.PostRepository
import com.aurora.blog.data.UserPost
import com.aurora.blog.data.UserRepository
import com.aurora.blog.utils.SingleLiveEvent
import com.aurora.blog.views.BaseViewModel
import com.aurora.blog.views.login.AuthenticationState
import kotlinx.coroutines.launch

class ProfileViewModel(user: String,
    private val userRepo: UserRepository,
    private val postRepo: PostRepository
) : BaseViewModel() {

    private val _authenticationState = MediatorLiveData<AuthenticationState>()
    val authenticationState: LiveData<AuthenticationState>
        get() = _authenticationState

    /** for menu*/
    private val _showUserMenu = SingleLiveEvent<Boolean>()
    val showUserMenu: LiveData<Boolean>
        get() = _showUserMenu

    val userName = MutableLiveData<String>()

    private val _showAddPost = SingleLiveEvent<Boolean>()
    val showAddPost: LiveData<Boolean>
        get() = _showAddPost

    private val userScreenState = MutableLiveData<UserScreenState>()

    init {
        /** the logged in user */
        val source = userRepo.getLoggedInUser()
        _authenticationState.apply {
            addSource(source) {
                removeSource(source)
                if (it != null) {
                    _authenticationState.value = AuthenticationState.AUTHENTICATED
                    if (user.trim().isEmpty() || user == it.userName) {
                        userName.value = it.userName
                        userScreenState.value = UserScreenState.ALL_OWNED
                        _showUserMenu.value = true
                    } else {
                        userName.value = user
                        userScreenState.value = UserScreenState.PUBLIC_OWNED
                        _showUserMenu.value = false
                    }
                } else {
                    _authenticationState.value = AuthenticationState.UNAUTHENTICATED
                }
            }
        }
    }

    /** get post applicable for the current user and screen*/
    val posts: LiveData<List<UserPost>> =  userScreenState.switchMap {
        when(it) {
            UserScreenState.ALL_OWNED -> postRepo.getUsersActivePosts(userName.value!!)
            UserScreenState.PUBLIC_OWNED -> postRepo.getUsersActivePosts(userName.value!!, false)
            else -> postRepo.getAllActivePublicPosts()
        }
    }

    fun onAddPostClicked() {
        _showAddPost.value = true
    }

    fun logout(): LiveData<Boolean> = liveData {
        userRepo.logOut()
        emit(true)
    }

    fun refresh() {
        // refresh
        userScreenState.value = userScreenState.value
    }

    fun deletePost(postId: Long) {
        viewModelScope.launch {
            postRepo.deletePost(postId)
            refresh()
        }
    }
}
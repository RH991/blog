package com.aurora.blog.views.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import com.aurora.blog.data.PostRepository
import com.aurora.blog.data.UserPost
import com.aurora.blog.data.UserRepository
import com.aurora.blog.utils.SingleLiveEvent
import com.aurora.blog.views.BaseViewModel
import kotlinx.coroutines.launch

class HomeViewModel(
    userRepo: UserRepository,
    private val postRepo: PostRepository
) : BaseViewModel() {

    private val _showAddPost = SingleLiveEvent<Boolean>()
    val showAddPost: LiveData<Boolean>
        get() = _showAddPost

    private val refresh = SingleLiveEvent<Boolean>()

    /** get post applicable for the current user and screen*/
    val posts: LiveData<List<UserPost>> = refresh.switchMap {
        userRepo.getLoggedInUser().switchMap {
            if (it != null) {
                postRepo.getAllPublicPostAndOwnedBy(it.userName)
            } else {
                postRepo.getAllActivePublicPosts()
            }
        }
    }

    fun onAddPostClicked() {
        _showAddPost.value = true
    }

    fun refresh() {
        refresh.value = true
    }

    fun deletePost(postId: Long) {
        viewModelScope.launch {
            postRepo.deletePost(postId)
            refresh()
        }
    }
}
package com.aurora.blog

import android.app.Application
import com.aurora.blog.di.dataModule
import com.aurora.blog.di.viewModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BlogApplication : Application() {
    override fun onCreate(){
        super.onCreate()
        startKoin {
            // declare used Android context
            androidContext(this@BlogApplication)
            // modules
            modules(listOf(dataModule, viewModule))
        }
    }
}
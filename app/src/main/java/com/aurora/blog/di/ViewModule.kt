package com.aurora.blog.di

import com.aurora.blog.data.UserPost
import com.aurora.blog.views.add.AddPostViewModel
import com.aurora.blog.views.home.HomeViewModel
import com.aurora.blog.views.login.LoginViewModel
import com.aurora.blog.views.profile.ProfileViewModel
import com.aurora.blog.views.signup.SignupViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModule = module {
    viewModel { HomeViewModel(get(), get()) }
    viewModel { LoginViewModel(get()) }
    viewModel { SignupViewModel(get()) }
    viewModel { (userPost: UserPost) -> AddPostViewModel(get(), get(), userPost) }
    viewModel { (user: String) -> ProfileViewModel(user, get(), get()) }
}
package com.aurora.blog.di

import androidx.room.Room
import com.aurora.blog.data.AppDatabase
import com.aurora.blog.data.PostRepository
import com.aurora.blog.data.UserRepository
import com.aurora.blog.utils.DATABASE_NAME
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.bind
import org.koin.dsl.module

val dataModule = module {
    /**
     * App room database
     */
    single { Room.databaseBuilder(androidContext(), AppDatabase::class.java, DATABASE_NAME)
        .fallbackToDestructiveMigration().build() } bind AppDatabase::class

    /**
     * Room Dao
     */
    factory { get<AppDatabase>().userDao() }

    factory { get<AppDatabase>().postDao() }

    /**
     * Repositories
     */
    single { PostRepository(get()) }

    single { UserRepository(get()) }
}

package com.aurora.blog.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
abstract class PostDao {

    /** insert a new post with image*/
    suspend fun createUserPost(userPost: UserPost): Long {
        return userPost.run {
            val id = insertPost(post)
            if (id > 0) {
                insertPostImages(images.apply {
                    forEach { it.fkPostId = id }
                })
            }
            id
        }
    }

    /** insert a new post*/
    @Insert
    abstract suspend fun insertPost(post: Post): Long

    /** insert post images*/
    @Insert
    abstract suspend fun insertPostImages(image: List<PostImage>)

    /** get the post by post ID */
    @Query("SELECT * FROM Post WHERE postId = :postId")
    abstract suspend fun getPost(postId: Long): Post?

    /** get all users active posts */
    @Transaction
    @Query("SELECT userName, p.* FROM Post p LEFT JOIN User " +
            "WHERE fkUserId = userId AND userName = :userName AND isActive = 1 " +
            "ORDER BY dateCreated DESC")
    abstract fun getUsersActivePosts(userName: String): LiveData<List<UserPost>>

    /** get all users all active post */
    @Transaction
    @Query("SELECT userName, p.* FROM Post p LEFT JOIN User WHERE fkUserId = userId " +
            "AND  isActive = 1 ORDER BY dateCreated DESC")
    abstract fun getAllActivePosts(): LiveData<List<UserPost>>

    /** get all users all active public post */
    @Transaction
    @Query("SELECT userName, p.* FROM Post p LEFT JOIN User WHERE fkUserId = userId " +
            "AND  isPrivate = 0 AND isActive = 1 ORDER BY dateCreated DESC")
    abstract fun getAllActivePublicPosts(): LiveData<List<UserPost>>

    /** update user post */
    @Update
    abstract suspend fun updatePost(post: Post): Int
}
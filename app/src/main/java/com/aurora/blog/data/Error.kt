package com.aurora.blog.data

enum class Error(val code: String) {
    ERROR_GENERIC("err_generic"),
    ERROR_USER_NAME_EXIST("err_user_name_exist"),
    ERROR_USER_EMAIL_EXIST("err_user_email_exist")
}
package com.aurora.blog.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.aurora.blog.utils.StringUtils.EMPTY

@Entity
data class User(
    @PrimaryKey(autoGenerate = true) val userId: Long = 0,
    val email: String,
    val userName: String,
    /** password has will be stored */
    var password: String,
    /** salt for password */
    var salt: String = EMPTY,
    val dateCreated: Long = System.currentTimeMillis())
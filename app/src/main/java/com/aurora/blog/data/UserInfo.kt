package com.aurora.blog.data

data class UserInfo(
    val userId: Long,
    val email: String,
    val userName: String)
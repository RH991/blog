package com.aurora.blog.data

import org.mindrot.jbcrypt.BCrypt

/**
 * Data source for [User] that handles authentication
 * and persisting the data
 *
 * @param dao the data access object for [User]
 */
class UserRepository(private val dao: UserDao) {

    /**
     * Checks the user if valid or not
     *
     * @param userName the username
     * @param password the plain text password
     * @return true if credentials is valid, otherwise return false
     */
    suspend fun isPasswordValidForUser(userName: String, password: String) : Boolean? {
        return dao.getUserByUserName(userName)?.run {
            if (dao.isUserNameAndPasswordValid(userName, BCrypt.hashpw(password, salt))) {
                dao.insertLogin(UserLogin(fkUserId = userId))
                true
            } else {
                false
            }
        }
    }

    /**
     * Create a new user
     *
     * @param user the user details
     * @return the user'd id
     */
    suspend fun signUp(user: User) = dao.createUser(user)

    /**
     * Log out active users
     *
     */
    suspend fun logOut() {
        dao.getAllActiveLogin().forEach {
            it.isActive = false
            it.dateLogout = System.currentTimeMillis()
            dao.updateUserLogin(it)
        }
    }

    /**
     * Get the logged in user
     *
     * @return the [UserInfo]
     */
    fun getLoggedInUser() = dao.getLoggedInUser()

}
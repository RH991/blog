package com.aurora.blog.data

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [User::class, UserLogin::class,
    Post::class, PostImage::class], version = 3, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun postDao(): PostDao
}
package com.aurora.blog.data

/**
 * Defines a status of a database operation
 */
enum class Status {
    SUCCESS,
    ERROR
}
package com.aurora.blog.data

import android.os.Parcelable
import androidx.room.*
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(
    foreignKeys = [ForeignKey(
        entity = User::class,
        parentColumns = ["userId"],
        childColumns = ["fkUserId"]
    )],
    indices = [Index("fkUserId")]
)
data class Post(
    @PrimaryKey(autoGenerate = true) val postId: Long = 0,
    val fkUserId: Long,
    val title: String,
    val content: String,
    /** indicates that the post is private or not */
    val isPrivate: Boolean,
    /** indicates that post is deleted or not */
    var isActive: Boolean = true,
    val dateCreated: Long = System.currentTimeMillis(),
    val dateUpdated: Long = System.currentTimeMillis()
) : Parcelable
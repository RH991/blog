package com.aurora.blog.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

/**
 * Data source for [Post] and [PostImage] that handles the retrieval
 * and persisting the data received from the user
 *
 * @param dao the data access object for [Post] and [PostImage]
 */
class PostRepository(private val dao: PostDao) {

    /**
     * Creates a user's [Post]
     *
     * @param userPost the post details to add
     * @return true if successful otherwise false
     */
    suspend fun addPost(userPost: UserPost) = dao.createUserPost(userPost) > 0

    /**
     * Update a user's [Post]
     *
     * @param post the post to update
     * @return true if successful otherwise false
     */
    suspend fun updatePost(post: Post) = dao.updatePost(post) > 0

    /**
     * Delete a user's [Post]
     *
     * @param postId the post ID
     * @return true if successful otherwise false
     */
    suspend fun deletePost(postId: Long) = dao.getPost(postId)?.let {
        it.isActive = false
        dao.updatePost(it) > 0
    }

    /**
     * Returns all active public [Post] including
     * the provided user's private [Post]
     */
    fun getAllPublicPostAndOwnedBy(userName: String): LiveData<List<UserPost>> {
        val result = MediatorLiveData<List<UserPost>>()
        val source = dao.getAllActivePosts()
        result.addSource(source) {
            result.removeSource(source)
            result.value = it.filter { p ->
                !p.post.isPrivate ||
                        (p.post.isPrivate && p.userName == userName)
            }.map { p ->
                p.editable = p.userName == userName
                p
            }.toList()
        }
        return result
    }

    /**
     * Returns all active [Post] of the provided user
     * with a additional privacy filter
     */
    fun getUsersActivePosts(
        userName: String,
        isPrivate: Boolean? = null
    ): LiveData<List<UserPost>> {
        val result = MediatorLiveData<List<UserPost>>()
        val source = dao.getUsersActivePosts(userName)
        result.addSource(source) {
            result.removeSource(source)
            val list = when (isPrivate) {
                true -> it.filter { u -> u.post.isPrivate }.toList()
                false -> it.filter { u -> !u.post.isPrivate }.toList()
                else -> it
            }
            result.value = list.map { p ->
                p.editable = p.userName == userName
                p
            }.toList()
        }
        return result
    }

    /**
     * Returns all active public [Post]
     */
    fun getAllActivePublicPosts() = dao.getAllActivePublicPosts()
}
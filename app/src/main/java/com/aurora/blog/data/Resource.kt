package com.aurora.blog.data

/**
 * A data holder with status
 * @param <T> the data type to hold
</T> */
data class Resource<out T>(val status: Status, val data: T?, val code: Error?) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(code: Error?): Resource<T> {
            return Resource(Status.ERROR, null, code)
        }
    }
}
package com.aurora.blog.data

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(foreignKeys =[ForeignKey(entity = Post::class, parentColumns = ["postId"], childColumns = ["fkPostId"])],
    indices = [Index("fkPostId")]
)
data class PostImage(
    @PrimaryKey(autoGenerate = true) val postImageId: Long = 0,
    var fkPostId: Long,
    /** used for ordering the images */
    val position: Int,
    /** image path saved in device */
    val uri: String) : Parcelable
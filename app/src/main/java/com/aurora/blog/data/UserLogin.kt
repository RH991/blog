package com.aurora.blog.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class UserLogin(
    @PrimaryKey(autoGenerate = true) val loginId: Long = 0,
    val fkUserId: Long,
    /** tracks the user if login or not */
    var isActive: Boolean = true,
    val dateLogin: Long = System.currentTimeMillis(),
    var dateLogout: Long = System.currentTimeMillis())
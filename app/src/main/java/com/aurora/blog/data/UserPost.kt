package com.aurora.blog.data

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Ignore
import androidx.room.Relation
import com.aurora.blog.utils.StringUtils.EMPTY
import com.aurora.blog.views.adapters.DiffCallback
import kotlinx.android.parcel.Parcelize

/**
 * This class captures the relationship between [Post] and [PostImage], which is
 * used by Room to fetch the related entities.
 */
@Parcelize
data class UserPost(
    val userName: String = EMPTY,
    @Embedded
    val post: Post,

    @Relation(parentColumn = "postId", entityColumn = "fkPostId")
    val images: List<PostImage> = emptyList()
): DiffCallback, Parcelable {
    override fun getId() = post.postId.toString()
    fun getSingleImage() = if (images.isEmpty()) EMPTY else images[0].uri
    @Ignore var editable: Boolean = false
}

package com.aurora.blog.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import org.mindrot.jbcrypt.BCrypt

@Dao
abstract class UserDao {

    /** Creates a user */
    suspend fun createUser(user: User): Resource<UserInfo> {
        /** check for existing user */
         if (getUserByUserName(user.userName) != null) return Resource.error(Error.ERROR_USER_NAME_EXIST)
         if (getUserByEmail(user.email) != null) return Resource.error(Error.ERROR_USER_EMAIL_EXIST)

        /** insert then verify */
        val userInfo = insertUser(user.apply {
            salt = BCrypt.gensalt()
            password = BCrypt.hashpw(password, salt)
        }).run { getUserInfo(toInt()) }
        if (userInfo != null) {
            insertLogin(UserLogin(fkUserId = userInfo.userId))
            return Resource.success(userInfo)
        }

        return Resource.error(Error.ERROR_GENERIC)
    }

    /** insert a new user*/
    @Insert
    abstract suspend fun insertUser(user: User): Long

    /** insert a new login, that tracks the current logged user*/
    @Insert
    internal abstract suspend fun insertLogin(login: UserLogin)

    /** get all active login's if possible*/
    @Query("SELECT *  FROM UserLogin WHERE isActive = 1")
    abstract suspend fun getAllActiveLogin(): List<UserLogin>

    /** get user's info*/
    @Query("SELECT userId, userName, email  FROM User WHERE userId = :userId")
    abstract suspend fun getUserInfo(userId: Int): UserInfo?

    /** get currently log user*/
    @Query("SELECT userId, userName, email  FROM User WHERE " +
            "userId IN (SELECT fkUserId FROM UserLogin WHERE isActive = 1 LIMIT 1)")
    abstract fun getLoggedInUser(): LiveData<UserInfo>

    /** insert a new post with image*/
    @Query("SELECT * FROM User WHERE userName = :userName")
    internal abstract suspend fun getUserByUserName(userName: String): User?

    /** get user by email*/
    @Query("SELECT * FROM User WHERE email = :email")
    internal abstract suspend fun getUserByEmail(email: String): User?

    /** check if the user credentials are valid*/
    @Query("SELECT EXISTS(SELECT 1 FROM User WHERE userName = :userName AND password = :password LIMIT 1)")
    abstract suspend fun isUserNameAndPasswordValid(userName: String, password: String): Boolean

    /** updates login details*/
    @Update
    internal abstract suspend fun updateUserLogin(user: UserLogin)
}
<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <import type="android.text.TextUtils" />

        <variable
            name="viewModel"
            type="com.aurora.blog.views.add.AddPostViewModel" />
    </data>

    <ScrollView
        android:id="@+id/scrollView"
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <androidx.constraintlayout.widget.ConstraintLayout
            android:layout_width="match_parent"
            android:layout_height="match_parent">

            <com.google.android.material.appbar.AppBarLayout
                android:id="@+id/appbar"
                android:layout_width="match_parent"
                android:layout_height="?attr/actionBarSize"
                app:layout_constraintTop_toTopOf="parent">

                <androidx.appcompat.widget.Toolbar
                    android:id="@+id/toolbar"
                    android:layout_width="match_parent"
                    android:layout_height="?attr/actionBarSize"
                    app:contentInsetStartWithNavigation="0dp"
                    app:layout_scrollFlags="scroll"
                    app:menu="@menu/menu_add_post"
                    app:navigationIcon="?attr/homeAsUpIndicator" />

            </com.google.android.material.appbar.AppBarLayout>

            <androidx.appcompat.widget.AppCompatImageView
                android:id="@+id/iv_avatar"
                android:layout_width="@dimen/avatar_size_small"
                android:layout_height="@dimen/avatar_size_small"
                android:layout_marginStart="@dimen/margin_normal"
                android:layout_marginTop="@dimen/margin_normal"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/appbar"
                app:srcCompat="@drawable/ic_default_avatar" />

            <androidx.appcompat.widget.AppCompatTextView
                android:id="@+id/tv_user"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginStart="@dimen/margin_normal"
                android:text="@{viewModel.userInfo.userName}"
                android:textSize="18sp"
                app:layout_constraintStart_toEndOf="@id/iv_avatar"
                app:layout_constraintTop_toTopOf="@id/iv_avatar"
                tools:text="rh991" />

            <androidx.appcompat.widget.AppCompatImageView
                android:id="@+id/iv_privacy"
                android:layout_width="@dimen/privacy_size"
                android:layout_height="@dimen/privacy_size"
                app:isPrivate="@{viewModel.isPrivate}"
                app:layout_constraintStart_toStartOf="@id/tv_user"
                app:layout_constraintTop_toBottomOf="@id/tv_user"
                app:srcCompat="@drawable/ic_public" />

            <androidx.appcompat.widget.AppCompatCheckBox
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginEnd="@dimen/margin_normal"
                android:button="@null"
                android:checked="@={viewModel.isPrivate()}"
                android:drawableEnd="?android:attr/listChoiceIndicatorMultiple"
                android:text="@string/private_post"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintTop_toTopOf="@id/tv_user" />

            <com.google.android.material.textfield.TextInputLayout
                android:id="@+id/til_title"
                style="@style/TextInputLayoutAppearance"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/margin_normal"
                android:layout_marginEnd="@dimen/margin_large"
                android:hint="@string/username"
                app:hintEnabled="false"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="@id/iv_avatar"
                app:layout_constraintTop_toBottomOf="@id/iv_avatar"
                app:titleValidator="@{viewModel.validationWatcher}">

                <com.google.android.material.textfield.TextInputEditText
                    style="@style/TextInputLayoutAppearance"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:hint="@string/give_me_title"
                    android:inputType="textMultiLine"
                    android:maxLength="50"
                    android:maxLines="3"
                    android:text="@={viewModel.title}"
                    android:textSize="16sp" />

            </com.google.android.material.textfield.TextInputLayout>

            <com.google.android.material.textfield.TextInputLayout
                android:id="@+id/til_content"
                style="@style/TextInputLayoutAppearance"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/margin_normal"
                android:hint="@string/password"
                app:hintEnabled="false"
                app:layout_constraintEnd_toEndOf="@id/til_title"
                app:layout_constraintStart_toStartOf="@id/til_title"
                app:layout_constraintTop_toBottomOf="@id/til_title"
                app:passwordToggleEnabled="true"
                app:passwordToggleTint="?android:textColor">

                <com.google.android.material.textfield.TextInputEditText
                    style="@style/TextInputLayoutAppearance"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:hint="@string/nice_content"
                    android:inputType="textMultiLine"
                    android:maxLength="100"
                    android:maxLines="6"
                    android:text="@={viewModel.content}" />

            </com.google.android.material.textfield.TextInputLayout>

            <androidx.appcompat.widget.AppCompatImageView
                android:id="@+id/iv_image"
                isGone="@{TextUtils.isEmpty(viewModel.image)}"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/margin_large"
                app:layout_constraintTop_toBottomOf="@id/til_content"
                tools:srcCompat="@drawable/ic_private"
                app:bindImage="@{viewModel.image}"
                tools:visibility="gone" />

            <androidx.appcompat.widget.AppCompatImageView
                android:id="@+id/iv_add_image"
                isGone="@{!TextUtils.isEmpty(viewModel.image)}"
                android:layout_width="@dimen/add_image_size"
                android:layout_height="@dimen/add_image_size"
                android:layout_marginTop="@dimen/margin_large"
                android:onClick="@{() -> viewModel.onImageAddClicked()}"
                app:layout_constraintEnd_toEndOf="@id/til_content"
                app:layout_constraintTop_toBottomOf="@id/til_content"
                app:srcCompat="@drawable/ic_image" />

        </androidx.constraintlayout.widget.ConstraintLayout>
    </ScrollView>
</layout>